package gl.ismasaga;

import gl.ismasaga.comunicacion.httpComunicacion.HttpController;
import gl.ismasaga.modelo.DispositivoVO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.UUID;

// Anotacion que aglomera multitude de cousas (@Configuration, @EnableAutoConfiguration, @ComponentScan)
// A grosso modo define esta clase como a principal e busca Beans a partires dela
@SpringBootApplication
public class RunwayIoTApplication {
    public static HashMap<String, DispositivoVO> listaDispositivos = new HashMap();

    public static void main(String[] args) {
        final String dirMqttServidor = "tcp://localhost:1883";
        final String temaBase = "zigbee2mqtt";
        final String idClienteMqtt = UUID.randomUUID().toString();
        final String dirGraphiteServidor = "localhost";
        final int portoServerGraphite = 2003;
        ControladorMedicions controladorMedicions;
        HttpController httpController;

        // Evita ter que facer un web.xml, 100% java (Arranca a API REST)
        ApplicationContext applicationContext = SpringApplication.run(RunwayIoTApplication.class, args);
        // Dispositivos
        httpController = applicationContext.getBean(HttpController.class);
        // Medicions
        controladorMedicions = new ControladorMedicions(dirMqttServidor, temaBase, idClienteMqtt,
                dirGraphiteServidor, portoServerGraphite, httpController);

/*
        // Primeira busca de dispositivos
        MulticastSocket enviador = null;
        DatagramPacket dgp = null;
        // Datos a mandar como array de bytes
        byte[] dato = new byte[]{'r', 'u', 'n', 'w', 'a', 'y', 'I', 'o', 'T'};

        // Crease o servidor para poder mandar mensaxes multicast
        try {
            enviador = new MulticastSocket();
        } catch (IOException e) {
            System.err.println("Erro creando o servidor multicast");
            e.printStackTrace();
        }
        // Crease o datagrama que se vai mandar usando SSDP, info en: https://es.wikipedia.org/wiki/SSDP
        try {
            dgp = new DatagramPacket(dato, dato.length, InetAddress.getByName("239.255.255.210"), 1900);
        } catch (UnknownHostException e) {
            System.err.println("Erro creando o datagrama");
            e.printStackTrace();
        }
        try {
            enviador.send(dgp);
        } catch (IOException e) {
            System.err.println("Erro enviando o datagrama");
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.err.println("Null pointer!!");
            e.printStackTrace();
        }
*/


    }


    // Os Bean executanse ao principio
    /*
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("Let's inspect the beans provided by Spring Boot:");
            // Recupera todos os Beans definidos na aplicacion
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            // Imprimeos por pantalla
            for (String beanName : beanNames)
                System.out.println(beanName);
        };
    }
    */
}
