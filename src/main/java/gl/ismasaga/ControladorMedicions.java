package gl.ismasaga;

import gl.ismasaga.comunicacion.httpComunicacion.HttpController;
import gl.ismasaga.comunicacion.mqttComunicacion.MqttAPI;
import gl.ismasaga.modelo.DispositivoVO;
import gl.ismasaga.modelo.RexistroVO;
import gl.ismasaga.modelo.TipoSensor;
import gl.ismasaga.persistencia.graphite.GraphiteDao;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class ControladorMedicions {
    private Logger LOG;
    private MqttAPI mqttController;
    private GraphiteDao graphiteDao;
    private HttpController httpController;

    public ControladorMedicions(String dirMqttServidor, String temaBase, String idClienteMqtt,
                                String dirGraphiteServidor, int portoServerGraphite, HttpController httpController) {
        this.LOG = LoggerFactory.getLogger(ControladorMedicions.class);
        mqttController = new MqttAPI(dirMqttServidor, temaBase, idClienteMqtt, this);
        mqttController.start();
        graphiteDao = new GraphiteDao(dirGraphiteServidor, portoServerGraphite);
        this.httpController = httpController;
    }

    public void decodificaMensaxe(String topic, MqttMessage mensaxe) {
        DispositivoVO dispositivoVO;

        LOG.info("Mensaxe recibido: " + topic + ": " + mensaxe.toString());
        String[] subtopics = topic.split("/");
        if (subtopics.length == 3) {
            if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && mensaxe.toString().equals("online")) {
                mqttController.setOnline(true);
                LOG.info("O coordinador ZigBee pasa a estar activo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && mensaxe.toString().equals("offline")) {
                mqttController.setOnline(false);
                LOG.warn("O coordinador ZigBee pasa a estar inactivo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("config"))
                LOG.info("Configuración do coordinador ZigBee(Topic '" + topic + "'):\n" + mensaxe.toString());
            else
                LOG.info("Formato de mensaxe non contemplado!!");
        } else if (subtopics.length == 2) {
            if (RunwayIoTApplication.listaDispositivos.containsKey(subtopics[1])) {
                dispositivoVO = RunwayIoTApplication.listaDispositivos.get(subtopics[1]);
                gardaMensaxe(dispositivoVO, mensaxe);
                httpController.autorizarRexistro(dispositivoVO);
            } else {
                dispositivoVO = new DispositivoVO(subtopics[1], true);
                RunwayIoTApplication.listaDispositivos.put(subtopics[1], dispositivoVO);
                gardaMensaxe(dispositivoVO, mensaxe);
                httpController.autorizarRexistro(dispositivoVO);
            }
        } else
            LOG.info("Formato de mensaxe non contemplado!!");
    }

    private void gardaMensaxe(DispositivoVO dispositivoVO, MqttMessage mensaxe) {
        RexistroVO rexistroVO = new RexistroVO(new Date(), dispositivoVO.getIdentificador());
        String[] aux = mensaxe.toString().replace("{", "").replace("}", "")
                .replaceAll("\"", "").split(",");
        String[] auxVal;

        for (String parte : aux) {
            System.out.println(parte);
            auxVal = parte.split(":");
            switch (auxVal[0]) {
                case "state":
                    //dispositivoVO.setNovaFuncion(auxVal[0], auxVal[1]);
                    dispositivoVO.setNovaFuncion(auxVal[1]);
                    break;
                case "linkquality":
                    dispositivoVO.setCalidadeEnlace(Integer.parseInt(auxVal[1]));
                    break;
                case "battery":
                    dispositivoVO.setBateria(Integer.parseInt(auxVal[1]));
                    break;
                case "voltage":
                    LOG.info("A voltaxe do dispositivo está en: " + auxVal[1]);
                    break;
                case "temperature":
                    dispositivoVO.setNovoSensor(TipoSensor.SensorTemperatura);
                    rexistroVO.setTipoMedida(TipoSensor.SensorTemperatura);
                    rexistroVO.setMagnitud("C");
                    rexistroVO.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistroVO);
                    break;
                case "humidity":
                    dispositivoVO.setNovoSensor(TipoSensor.SensorHumidade);
                    rexistroVO.setTipoMedida(TipoSensor.SensorHumidade);
                    rexistroVO.setMagnitud("%");
                    rexistroVO.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistroVO);
                    break;
                case "pressure":
                    dispositivoVO.setNovoSensor(TipoSensor.SensorPresionAtmosferica);
                    rexistroVO.setTipoMedida(TipoSensor.SensorPresionAtmosferica);
                    rexistroVO.setMagnitud("mb");
                    rexistroVO.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistroVO);
                    break;
                case "power":
                    dispositivoVO.setNovoSensor(TipoSensor.SensorConsumoElectrico);
                    rexistroVO.setTipoMedida(TipoSensor.SensorConsumoElectrico);
                    rexistroVO.setMagnitud("W");
                    rexistroVO.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistroVO);
                    break;
                default:
                    LOG.error("Mensaxe MQTT inesperado");
                    break;
            }
        }
    }
}
