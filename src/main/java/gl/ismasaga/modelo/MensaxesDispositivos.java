package gl.ismasaga.modelo;

/**
 * Enumeración dos tipos de menxaxes disponibles que manexa o concentrador para comunicarse cos dispositivos
 */
public final class MensaxesDispositivos {
    public static final String PERMITIR_DISPOSITIVO = "PASS";
    public static final String NOVO_REXISTRO_CORRECTO = "OK";
}
