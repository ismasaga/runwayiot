package gl.ismasaga.modelo;

import gl.ismasaga.persistencia.graphite.GraphiteDao;

import java.util.HashSet;

public class DispositivoMqtt extends SolicitudeRexistro {
    private String renombreDispositivo;
    private GraphiteDao graphiteDao;
    private boolean estado;
    private int calidadSenhal, bateria, voltaxe;

    public DispositivoMqtt(String identificador, HashSet tipos, String nomeDispositivo,
                           String renombreDispositivo) {
        super(identificador, tipos, nomeDispositivo);
        this.renombreDispositivo = renombreDispositivo;
    }

    /*public DispositivoMqtt(String identificador, MqttMessage mensaxe, GraphiteDao graphiteDao) {
        this.setIdentificador(identificador);
        this.graphiteDao = graphiteDao;
        parseaMensaxe(mensaxe);
    }

    public void parseaMensaxe(MqttMessage mensaxe) {
        String[] aux = mensaxe.toString().replace("{", "").replace("}", "")
                .replaceAll("\"", "").split(",");
        String[] auxVal;
        Rexistro rexistro = new Rexistro(this.getIdentificador(), new Date());

        for (String parte : aux) {
            System.out.println(parte);
            auxVal = parte.split(":");
            switch (auxVal[0]) {
                case "state":
                    estado = auxVal[1].equals("ON");
                    break;
                case "linkquality":
                    calidadSenhal = Integer.parseInt(auxVal[1]);
                    break;
                case "battery":
                    bateria = Integer.parseInt(auxVal[1]);
                    break;
                case "voltage":
                    bateria = Integer.parseInt(auxVal[1]);
                    break;
                case "temperature":
                    this.addTipo(TipoDispositivo.SensorTemperatura);
                    rexistro.setTipo(TipoDispositivo.SensorTemperatura);
                    rexistro.setMagnitud("C");
                    rexistro.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistro);
                    break;
                case "humidity":
                    this.addTipo(TipoDispositivo.SensorHumidade);
                    rexistro.setTipo(TipoDispositivo.SensorHumidade);
                    rexistro.setMagnitud("%");
                    rexistro.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistro);
                    break;
                case "pressure":
                    this.addTipo(TipoDispositivo.SensorPresionAtmosferica);
                    rexistro.setTipo(TipoDispositivo.SensorPresionAtmosferica);
                    rexistro.setMagnitud("mb");
                    rexistro.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistro);
                    break;
                case "power":
                    this.addTipo(TipoDispositivo.SensorConsumoElectrico);
                    rexistro.setTipo(TipoDispositivo.SensorConsumoElectrico);
                    rexistro.setMagnitud("W");
                    rexistro.setValorNumerico(Double.parseDouble(auxVal[1]));
                    graphiteDao.save(rexistro);
                    break;
                default:
                    System.err.println("Mensaxe MQTT inesperado");
                    break;
            }
        }
    }

    public boolean novoRexistro(Rexistro rexistro) {
        return true;
    }*/
}
