package gl.ismasaga.modelo;

public enum TipoSensor {
    SensorTemperatura,
    SensorHumidade,
    SensorPresionAtmosferica,
    SensorConsumoElectrico,
    InterruptorIntelixente/*,    Irase descomentando conforme se implementen
    SensorLuminosidade,
    SensorCO2,
    SensorInundacion,
    Higrometro, // TODO: Diferencia co de humidade relativa?
    SensorProducionElectrica,
    SensorXenerico*/
}
