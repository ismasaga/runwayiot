package gl.ismasaga.modelo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashSet;

@Document(collection = "dispositivos")
public class DispositivoVO extends SolicitudeRexistro {
    private Date dataRexistro;
    //private HashMap<String, String> listaFuncions;
    private HashSet<TipoSensor> listaSensores;
    private boolean estado;
    private int bateria;
    private float calidadeEnlace;

    /** Non pode existir un dispositivo sen identificador e data de rexistro
     *
     * @param identificador Valor que identifica univocamente a un dispositivo
     * @param estado Representa se o dispositivo se atopa conectado nun intre determinado
     */
    public DispositivoVO(String identificador, boolean estado) {
        super.setIdentificador(identificador);
        super.setNomeDispositivo("Sensor");
        this.estado = estado;
        this.dataRexistro = new Date();
        super.setFuncionalidade("Sen funcións");
    }

    public DispositivoVO() {}

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getIdentificador() {
        return super.getIdentificador();
    }

    public void setNovaFuncion(String funcion) {
        super.setFuncionalidade(funcion);
    }

    /** Rexistra un tipo de sensor para este dispositivo
     *
     * @param tipoSensor Os tipos de sensores permitidos ver: TipoSensor.java
     * @return Se o tipo de sensor non estaba rexistrado true, se xa era conhecido false
     */
    public boolean setNovoSensor(TipoSensor tipoSensor) {
        if (this.listaSensores == null)
            this.listaSensores = new HashSet<>();
        return this.listaSensores.add(tipoSensor);
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public void setCalidadeEnlace(float calidadeEnlace) {
        this.calidadeEnlace = calidadeEnlace;
    }

    /**
     * Crea un string formateado para a impresion dos datos dun dispositivo
     *
     * @return String formateado
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n\tIdentificador (MAC)   : ").append(super.getIdentificador());
        builder.append("\n\tNome dispositivo      : ").append(super.getNomeDispositivo());
        builder.append("\n\tTipos dispositivo      : ");
        for (TipoDispositivo tipo : super.getTipos())
            builder.append("\n\t\t").append(tipo);
        /*if (listaFuncions != null && !listaFuncions.isEmpty())
            builder.append("\n\tFuncións dispositivo  : ").append(this.listaFuncions);*/
        builder.append("\n\tFuncionalidade: ").append(super.getFuncionalidade());
        return builder.toString();
    }
}
