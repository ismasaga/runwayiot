package gl.ismasaga.modelo;

/**
 * Enumeración dos tipos de dipositivos disponibles que manexa o concentrador
 */
public enum TipoDispositivo {
    SensorTemperatura,
    SensorHumidade,
    SensorPresionAtmosferica,
    SensorConsumoElectrico,
    InterruptorIntelixente/*,    Irase descomentando conforme se implementen
    SensorLuminosidade,
    SensorCO2,
    SensorInundacion,
    Higrometro, // TODO: Diferencia co de humidade relativa?
    SensorProducionElectrica,
    SensorXenerico*/
}
