package gl.ismasaga.modelo;

import org.springframework.data.annotation.Id;

import java.util.HashSet;

/**
 * Clase que modela unha solicitude de rexistro de un dispositivo no concentrador
 */
public class SolicitudeRexistro {
    @Id
    private String identificador; // O tipo de dato ocupa 64 bits inda que unha direccion MAC so 48
    private HashSet<TipoDispositivo> tipos;
    private String nomeDispositivo;
    private String funcionalidade;

    public String getFuncionalidade() {
        return funcionalidade;
    }

    public void setFuncionalidade(String funcionalidade) {
        this.funcionalidade = funcionalidade;
    }

    public SolicitudeRexistro(String identificador, HashSet<TipoDispositivo> tipos, String nomeDispositivo) {
        this.identificador = identificador;
        this.tipos = tipos;
        this.nomeDispositivo = nomeDispositivo;
    }

    public SolicitudeRexistro() {
        tipos = new HashSet<>();
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public HashSet<TipoDispositivo> getTipos() {
        return tipos;
    }

    public void addTipo(TipoDispositivo tipo) {
        this.tipos.add(tipo);
    }

    public String getNomeDispositivo() {
        return nomeDispositivo;
    }

    public void setNomeDispositivo(String nomeDispositivo) {
        this.nomeDispositivo = nomeDispositivo;
    }

    /**
     * Imprime a solicitude de rexistro dun dispositivo
     * @return cadea formateada
     */
    @Override
    public String toString() {
        StringBuilder cadea = new StringBuilder();

        cadea.append("Imprimindo dispositivo...\n");
        if (nomeDispositivo == null || nomeDispositivo.isEmpty())
            cadea.append("DispositivoHttp sen nomear\n");
        else
            cadea.append("[").append(nomeDispositivo).append("] ");
        cadea.append(identificador).append(" :\n");
        for (TipoDispositivo tipo : tipos)
            cadea.append("\t").append(tipo.toString()).append("\n");
        return cadea.toString();
    }
}
