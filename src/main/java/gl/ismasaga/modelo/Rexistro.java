package gl.ismasaga.modelo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Clase que modela un rexistro ou entrada de datos
 */
@Document(collection = "rexistros")
public final class Rexistro {
    private TipoDispositivo tipo;
    private String magnitud;
    private Double valorNumerico;
    private String valorTexto;
    private Date fecha;
    private String idDispositivo;
    private String idSensor;

    public Rexistro(TipoDispositivo tipo, String magnitud, Double valorNumerico, String valorTexto, Date fecha, String idDispositivo, String idSensor) {
        this.tipo = tipo;
        this.magnitud = magnitud;
        this.valorNumerico = valorNumerico;
        this.valorTexto = valorTexto;
        this.fecha = fecha;
        this.idDispositivo = idDispositivo;
        this.idSensor = idSensor;
    }

    public Rexistro(String idDispositivo, Date fecha) {
        this.idDispositivo = idDispositivo;
        this.fecha = fecha;
    }

    public Rexistro() {}

    public TipoDispositivo getTipo() {
        return tipo;
    }

    public void setTipo(TipoDispositivo tipo) {
        this.tipo = tipo;
    }

    public String getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(String magnitud) {
        this.magnitud = magnitud;
    }

    public Double getValorNumerico() {
        return valorNumerico;
    }

    public void setValorNumerico(Double valorNumerico) {
        this.valorNumerico = valorNumerico;
    }

    public String getValorTexto() {
        return valorTexto;
    }

    public void setValorTexto(String valorTexto) {
        this.valorTexto = valorTexto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public String getIdSensor() {
        return idSensor;
    }

    public void setIdSensor(String idSensor) {
        this.idSensor = idSensor;
    }

    @Override
    public String toString() {
        return "Rexistro{" +
                "tipo='" + tipo + '\'' +
                ", magnitud='" + magnitud + '\'' +
                ", valorNumerico=" + valorNumerico +
                ", valorTexto='" + valorTexto + '\'' +
                ", fecha=" + fecha +
                ", idDispositivo='" + idDispositivo + '\'' +
                ", idSensor='" + idSensor + '\'' +
                '}';
    }
}