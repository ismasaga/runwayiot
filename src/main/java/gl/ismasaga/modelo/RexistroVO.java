package gl.ismasaga.modelo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "rexistros")
public class RexistroVO {
    private Date fecha;
    private String idDispositivo;
    private TipoSensor tipoMedida;
    private String magnitud;
    private Double valorNumerico;
    private String valorTexto;
    private String idSensor;

    public RexistroVO(Date fecha, String idDispositivo) {
        this.fecha = fecha;
        this.idDispositivo = idDispositivo;
    }

    public void setTipoMedida(TipoSensor tipoMedida) {
        this.tipoMedida = tipoMedida;
    }

    public TipoSensor getTipoMedida() {
        return this.tipoMedida;
    }

    public void setMagnitud(String magnitud) {
        this.magnitud = magnitud;
    }

    public void setValorNumerico(Double valorNumerico) {
        this.valorNumerico = valorNumerico;
    }

    public Double getValorNumerico() {
        return this.valorNumerico;
    }

    public String getIdDispositivo() {
        return this.idDispositivo;
    }

    @Override
    public String toString() {
        return "Rexistro{" +
                "tipo='" + tipoMedida + '\'' +
                ", magnitud='" + magnitud + '\'' +
                ", valorNumerico=" + valorNumerico +
                ", valorTexto='" + valorTexto + '\'' +
                ", fecha=" + fecha +
                ", idDispositivo='" + idDispositivo + '\'' +
                ", idSensor='" + idSensor + '\'' +
                '}';
    }
}