package gl.ismasaga.persistencia.mongodb;

import gl.ismasaga.modelo.DispositivoVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Repositorio que manexa os dispositivos
 */
public interface DispositivosRepository extends MongoRepository<DispositivoVO, String> {
    // Tenta gardar un dispositivo sen comprobar se existia previamente
    DispositivoVO save(DispositivoVO dispositivo);
    // Comproba se existe un determinado dispositivo (o identificador e a MAC)
    boolean existsDispositivoByIdentificador(String identificador);
    // Devolve o dispositivo con MAC
    DispositivoVO findDispositivoByIdentificador(String identificador);
    // Devolve todos os dispositivos rexistrados no concentrador
    List<DispositivoVO> findAll();
    // Devolve todos os dispositivos rexistrados no concentrador paxinados
    Page<DispositivoVO> findAll(Pageable pageable);
    // Elimina un dispositivo (o identificador e a MAC)
    void deleteDispositivoByIdentificador(String identificador);
}
