package gl.ismasaga.persistencia.graphite;

import gl.ismasaga.modelo.RexistroVO;
import gl.ismasaga.persistencia.IDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.List;
import java.util.Optional;

public class GraphiteDao implements IDao<RexistroVO> {
    private final String host;
    private final int port;
    private Logger LOG;

    public GraphiteDao(String host, int port) {
        this.host = host;
        this.port = port;
        this.LOG = LoggerFactory.getLogger(GraphiteDao.class);
    }

    @Override
    public void save(RexistroVO rexistroVO) {
        Socket socket;
        StringBuilder mensaxe = new StringBuilder();

        try {
            socket = new Socket(host, port);
        } catch (IOException e) {
            System.err.println("Erro creando o socket de Graphite!!");
            socket = null;
            e.printStackTrace();
        }
        try (
                Writer writer = new OutputStreamWriter(socket.getOutputStream())) {
            mensaxe.append("dispositivos.").append(rexistroVO.getIdDispositivo()).append(".")
                    .append(rexistroVO.getTipoMedida().toString()).append(" ").append(rexistroVO.getValorNumerico())
                    .append(" ").append(System.currentTimeMillis() / 1000).append("\n");
            LOG.info("Gardando en graphite: " + mensaxe.toString());
            writer.write(mensaxe.toString());
            writer.flush();
        } catch (IOException e) {
            LOG.error("Erro enviando mensaxes a Graphite");
            e.printStackTrace();
        }
    }

    @Override
    public boolean exists(RexistroVO rexistroVO) {
        LOG.warn("Sen funcionalidade!!");
        return false;
    }

    @Override
    public Optional<RexistroVO> findById(long id) {
        LOG.warn("Sen funcionalidade!!");
        return Optional.empty();
    }

    @Override
    public List<RexistroVO> findAll() {
        LOG.warn("Sen funcionalidade!!");
        return null;
    }

    @Override
    public void update(RexistroVO rexistroVO, String[] params) {
        LOG.warn("Sen funcionalidade!!");
    }

    @Override
    public void delete(RexistroVO rexistroVO) {
        LOG.warn("Sen funcionalidade!!");
    }
}
