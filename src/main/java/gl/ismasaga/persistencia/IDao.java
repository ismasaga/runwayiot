package gl.ismasaga.persistencia;

import java.util.List;
import java.util.Optional;

public interface IDao<T> {
    void save(T t);

    boolean exists(T t);

    Optional<T> findById(long id);

    List<T> findAll();

    void update(T t, String[] params);

    void delete(T t);
}
