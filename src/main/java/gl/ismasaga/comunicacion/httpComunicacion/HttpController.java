package gl.ismasaga.comunicacion.httpComunicacion;

import gl.ismasaga.modelo.DispositivoVO;
import gl.ismasaga.modelo.MensaxesDispositivos;
import gl.ismasaga.modelo.Rexistro;
import gl.ismasaga.modelo.SolicitudeRexistro;
import gl.ismasaga.persistencia.mongodb.DispositivosRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.UUID;

// Habilita o manexo de peticions web (Spring MVC) combina @Controller
// e @ResponseBody que fai que reponda texto en vez de unha vista
@RestController
// Asigna como raiz do controlador a ruta /v1
@RequestMapping(value = "/v1")
public class HttpController {
    private ArrayList<SolicitudeRexistro> solicitudes;
    private DispositivosRepository dispRepo;
    private Logger LOG;

    @Autowired
    public HttpController(DispositivosRepository dispRepo) {
        this.dispRepo = dispRepo;
        this.LOG = LoggerFactory.getLogger(HttpController.class);
        solicitudes = new ArrayList<>();
    }

    //TODO:Ir engadindo Swagger (engadir respostas e validar na UI) e reformatear as respostas en cada metodo do dontrolador
    // Mapea a chamada a .../v1/ como a chamada a este metodo
    @RequestMapping(path = "/buscar")
    @CrossOrigin
    public ResponseEntity buscarDispositivos() {
        MulticastSocket enviador = null;
        DatagramPacket dgp = null;
        // Datos a mandar como array de bytes
        byte[] dato = new byte[]{'r', 'u', 'n', 'w', 'a', 'y', 'I', 'o', 'T'};

        // Crease o servidor para poder mandar mensaxes multicast
        try {
            enviador = new MulticastSocket();
        } catch (IOException e) {
            System.err.println("Erro creando o servidor multicast");
            e.printStackTrace();
        }
        // Crease o datagrama que se vai mandar usando SSDP, info en: https://es.wikipedia.org/wiki/SSDP
        try {
            dgp = new DatagramPacket(dato, dato.length, InetAddress.getByName("239.255.255.210"), 1900);
        } catch (UnknownHostException e) {
            System.err.println("Erro creando o datagrama");
            e.printStackTrace();
        }
        // Envio do datagrama 3 veces
        for (int i = 0; i < 3; i++) {
            try {
                enviador.send(dgp);
            } catch (IOException e) {
                System.err.println("Erro enviando o datagrama");
                e.printStackTrace();
            } catch (NullPointerException e) {
                System.err.println("Null pointer!!");
                e.printStackTrace();
            }
            try {
                System.out.println("Esperando 1 sec. ata proseguir.");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Erro facendo a espera");
                e.printStackTrace();
            }
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "/solicituderexistro",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @CrossOrigin
    public String solicitudeRexistro(@RequestBody SolicitudeRexistro solicitudeRexistro) {
        final String idDispositivoSolicitante = solicitudeRexistro.getIdentificador();
        LOG.info("Tentando rexistrarse o dispositivo: " + idDispositivoSolicitante);

        if (solicitudes.size() < 20) {
            // Comprobase se a solicitude xa foi rexistrada con anterioridade
            for (SolicitudeRexistro solicitudeRexistroAuxiliar : solicitudes) {
                if (solicitudeRexistroAuxiliar.getIdentificador().equals(idDispositivoSolicitante)) {
                    LOG.warn("A solicitude do dispositivo con identificador " +
                            solicitudeRexistro.getNomeDispositivo() +
                            " xa fora rexistrada con anterioridade");
                    return "Solicitude xa rexistrada";
                }
            }
            // Comprobase se o dispositivo xa estaba rexistrado previamente para poñelo en funcionamento
            if (dispRepo.existsDispositivoByIdentificador(idDispositivoSolicitante)) {
                LOG.info(solicitudeRexistro.getNomeDispositivo() + " xa previamente rexistrado");
                return MensaxesDispositivos.PERMITIR_DISPOSITIVO;
            } else {
                solicitudes.add(solicitudeRexistro);
                return "Solicitude rexistrada con éxito";
            }
        } else {
            return "Máximo de solicitudes de rexistro no concentrador acadado";
        }
    }

    @ApiOperation("Rexistra un determinado dispositivo no sistema")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Novo dispositivo rexistrado"),
            @ApiResponse(code = 409, message = "O dispositivo con ese identificador (MAC) xa se atopa rexistrado")
    })
    @PostMapping(
            path = "/autorizarrexistro",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED) // Por defecto swagger inclue o 200, isto fai que por defecto sexa 201
    @CrossOrigin // Permite as orixes cruzadas, sen ela non se poden facer peticions dende o exterior
    public ResponseEntity autorizarRexistro(@RequestBody DispositivoVO dispositivo) {
        final String idDispositivo = dispositivo.getIdentificador();
        // Comprobase se o dispositivo estaba rexistrado previamente
        if (dispRepo.existsDispositivoByIdentificador(idDispositivo)) {
            LOG.warn("Tentando volver insertar o dispositivo:" + dispositivo.toString());
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
            // Se non estaba rexistrado gardase na BdFacade (rexistrase) e devolvese o CREATED correspondente
        } else {
            //dispositivo.setFechaRexistro(new Date()); // Dada na creacion do dispositivo ACTUALMENTE
            dispRepo.save(dispositivo);
            LOG.info("Insertado na BdFacade un novo dispositivo:" + dispositivo.toString());
            // Eliminamos a solicitude do dispositivo xa rexistrado
            for (SolicitudeRexistro solicitude : solicitudes) {
                if (solicitude.getIdentificador().equals(idDispositivo)) {
                    solicitudes.remove(solicitude);
                    LOG.info("Eliminada a solicitude  do dispositivo " + idDispositivo);
                }
            }
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @GetMapping(path = "/listarsolicitudesProba",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String listarSolicitudesProba() {
        System.out.println(solicitudes.toString());
        return solicitudes.toString();
    }

    @GetMapping(path = "/listarsolicitudes",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @CrossOrigin // TODO: configurar o CORS como no proxecto de AOS
    public ArrayList<SolicitudeRexistro> listarSolicitudes() {
        System.out.println(solicitudes.toString());
        return solicitudes;
    }

    @GetMapping(path = "/dispositivos",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @CrossOrigin // TODO: configurar o CORS como no proxecto de AOS
    public ArrayList<DispositivoVO> listarDispositivos() {
        return new ArrayList<>(dispRepo.findAll());
    }

    @ApiOperation("Crea un novo rexistro no sistema")
    /*@ApiResponses({
            @ApiResponse(code = 201, message = "Novo rexistro engadido"),
            @ApiResponse(code = 403, message = "O dispositivo con ese identificador (MAC) non se atopa rexistrado"),
            @ApiResponse(code = 409, message = "Precisase o identificador do dispositivo que tenta crear o rexistro!!")
    })*/
    @PostMapping(
            path = "/novorexistro",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED) // Por defecto swagger inclue o 200, isto fai que por defecto sexa 201
    @CrossOrigin // Permite as orixes cruzadas, sen ela non se poden facer peticions dende o exterior
    public String novoRexistro(@RequestBody Rexistro rexistro) {
        if (rexistro.getIdDispositivo() != null || rexistro.getIdSensor() != null) {
            LOG.info("Entrou polo if");
            if (dispRepo.existsDispositivoByIdentificador(rexistro.getIdDispositivo())) {
                //graphiteDao.save(rexistro); // Usado para pintar datos en Graphite dende un dispositivo Http
                return MensaxesDispositivos.NOVO_REXISTRO_CORRECTO;
            } else
                return "DispositivoHttp sen rexistrar";
        } else
            LOG.info("Entrou polo else");
        // Conflicto por vir un destes campos a nulo
        return "Rexistro mal formado";
    }

    @ApiOperation("Elimina un dispositivo do concentrador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "DispositivoHttp eliminado correctamente"),
            @ApiResponse(code = 202, message = "O dispositivo con ese identificador (MAC) non se atopa rexistrado"),
            @ApiResponse(code = 400, message = "Precisase o identificador do dispositivo que se tenta eliminar")
    })
    @DeleteMapping(
            path = "/dispositivo"
    )
    @CrossOrigin // Permite as orixes cruzadas, sen ela non se poden facer peticions dende o exterior
    public ResponseEntity eliminarDispositivo(@RequestParam String identificador) {
        LOG.info("Debug: " + identificador);
        if (identificador == null || identificador.trim().isEmpty())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        if (dispRepo.existsDispositivoByIdentificador(identificador)) {
            dispRepo.deleteDispositivoByIdentificador(identificador);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @ApiOperation("Cambia o estado do interruptor")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Novo dispositivo rexistrado"),
            @ApiResponse(code = 409, message = "O dispositivo con ese identificador (MAC) xa se atopa rexistrado")
    })
    @PostMapping(
            path = "/cambiarestado",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED) // Por defecto swagger inclue o 200, isto fai que por defecto sexa 201
    @CrossOrigin // Permite as orixes cruzadas, sen ela non se poden facer peticions dende o exterior
    public ResponseEntity cambiarEstadoInterruptor(@RequestBody SolicitudeRexistro dispositivo) {
        /*final String idDispositivo = dispositivo.getIdentificador();
        // Comprobase se o dispositivo estaba rexistrado previamente
        if (dispRepo.existsDispositivoByIdentificador(idDispositivo)) {
            LOG.warn("Tentando volver insertar o dispositivo:" + dispositivo.toString());
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
            // Se non estaba rexistrado gardase na BdFacade (rexistrase) e devolvese o CREATED correspondente
        } else {
            //dispositivo.setFechaRexistro(new Date()); // Dada na creacion do dispositivo ACTUALMENTE
            dispRepo.save(dispositivo);
            LOG.info("Insertado na BdFacade un novo dispositivo:" + dispositivo.toString());
            // Eliminamos a solicitude do dispositivo xa rexistrado
            for (SolicitudeRexistro solicitude : solicitudes) {
                if (solicitude.getIdentificador().equals(idDispositivo)) {
                    solicitudes.remove(solicitude);
                    LOG.info("Eliminada a solicitude  do dispositivo " + idDispositivo);
                }
            }*/
        LOG.info("Recibido");
        DispositivoVO dispositivoVO= dispRepo.findDispositivoByIdentificador(dispositivo.getIdentificador());
        MqttClient escritor = null;
        try {
            escritor = new MqttClient("tcp://127.0.0.1:1883", UUID.randomUUID().toString(), null);
            escritor.connect();
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
        MqttMessage message;
        if (dispositivo.getFuncionalidade().equals("OFF")) {
            message = new MqttMessage("{\"state\": \"ON\"}".getBytes());
            dispositivoVO.setFuncionalidade("ON");
        } else {
            message = new MqttMessage("{\"state\": \"OFF\"}".getBytes());
            dispositivoVO.setFuncionalidade("OFF");
        }
        message.setQos(0);
        System.out.println("Enviando a mensaxe: " + message.toString());
        try {
            escritor.publish("zigbee2mqtt-topic/" + dispositivo.getIdentificador() + "/set", message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
        System.out.println("Message published");
        dispRepo.save(dispositivoVO);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
