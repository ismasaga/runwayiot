package gl.ismasaga.comunicacion.mqttComunicacion;

import gl.ismasaga.persistencia.graphite.GraphiteDao;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;

public class SubscribeCallback implements MqttCallback {
    private boolean online;
    private HashMap listaDispositivos;
    private GraphiteDao graphiteDao;

    public SubscribeCallback(boolean online, HashMap listaDispositivos, GraphiteDao graphiteDao) {
        this.online = online;
        this.listaDispositivos = listaDispositivos;
        this.graphiteDao = graphiteDao;
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.err.println("Conexión perdida");
        cause.printStackTrace();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        /*String[] subtopics = topic.split("/");
        DispositivoMqtt dispositivoMqtt;

        System.out.println(topic + ": " + message.toString());
        if (subtopics.length == 3) {
            if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && message.toString().equals("online")) {
                online = true;
                System.out.println("O coordinador ZigBee pasa a estar activo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && message.toString().equals("offline")) {
                online = false;
                System.out.println("O coordinador ZigBee pasa a estar inactivo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("config"))
                System.out.println("Configuración do coordinador ZigBee(Topic '" + topic + "'):\n" + message.toString());
            else
                System.err.println("Erro parseando as mensaxes do topic: " + topic);
        } else if (subtopics.length == 2) {
            if (listaDispositivos.containsKey(subtopics[1])) {
                dispositivoMqtt = new DispositivoMqtt(subtopics[1], message, graphiteDao);
            } else {
                dispositivoMqtt = new DispositivoMqtt(subtopics[1], message, graphiteDao);
                //listaDispositivos.put(subtopics[1], dispositivoMqtt);
            }
        }*/
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("Entrega terminada");
    }

}