package gl.ismasaga.comunicacion.mqttComunicacion;

import gl.ismasaga.modelo.DispositivoMqtt;
import gl.ismasaga.persistencia.graphite.GraphiteDao;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.HashMap;

public class MqttController {
    private boolean online;
    private final String BASE_TOPIC_NAME;
    private MqttClient client;
    private HashMap<String, DispositivoMqtt> listaDispositivos;
    private GraphiteDao graphiteDao;

    public MqttController(String brokerUrl, String BASE_TOPIC_NAME, String clientId) {
        this.BASE_TOPIC_NAME = BASE_TOPIC_NAME;
        try {
            this.client = new MqttClient(brokerUrl, clientId);
        } catch (MqttException e) {
            System.err.println("Erro ao crear o cliente MQTT!!");
            this.client = null;
            e.printStackTrace();
        }
        listaDispositivos = new HashMap<>();
        this.graphiteDao = new GraphiteDao("localhost", 2003);
    }

    public void start() {
        if (client != null) {
            client.setCallback(new SubscribeCallback(online, listaDispositivos, graphiteDao));
            try {
                client.connect();
            } catch (MqttException e) {
                System.err.println("O cliente MQTT non se logrou conectar correctamente!!");
                e.printStackTrace();
            }
            try {
                client.subscribe(BASE_TOPIC_NAME + "/#");
            } catch (MqttException e) {
                System.err.println("O cliente MQTT non se logrou suscribir ao topic " + BASE_TOPIC_NAME);
                e.printStackTrace();
            }
        } else {
            System.err.println("O cliente MQTT non foi creado correctamente!!");
        }
    }
}
