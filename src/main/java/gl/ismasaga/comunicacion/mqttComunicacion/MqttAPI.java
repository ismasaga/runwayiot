package gl.ismasaga.comunicacion.mqttComunicacion;

import gl.ismasaga.ControladorMedicions;
import org.eclipse.paho.client.mqttv3.*;

public class MqttAPI {
    private final String temaBase;
    private MqttClient client;
    private ControladorMedicions controladorMedicions;
    private boolean online;

    public MqttAPI(String brokerUrl, String temaBase, String idClienteMqtt, ControladorMedicions controladorMedicions) {
        this.temaBase = temaBase;
        this.controladorMedicions = controladorMedicions;
        try {
            this.client = new MqttClient(brokerUrl, idClienteMqtt);
        } catch (MqttException e) {
            System.err.println("Erro ao crear o cliente MQTT!!");
            this.client = null;
            e.printStackTrace();
        }
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public void start() {
        if (client != null) {
            client.setCallback(new DetectorMensaxes(controladorMedicions));
            try {
                client.connect();
            } catch (MqttException e) {
                System.err.println("O cliente MQTT non se logrou conectar correctamente!!");
                e.printStackTrace();
            }
            try {
                client.subscribe(temaBase + "/#");
            } catch (MqttException e) {
                System.err.println("O cliente MQTT non se logrou suscribir ao topic " + temaBase);
                e.printStackTrace();
            }
        } else {
            System.err.println("O cliente MQTT non foi creado correctamente!!");
        }
    }
}

class DetectorMensaxes implements MqttCallback {
    private ControladorMedicions controladorMedicions;

    public DetectorMensaxes(ControladorMedicions controladorMedicions) {
        this.controladorMedicions = controladorMedicions;
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.err.println("Conexión perdida");
        cause.printStackTrace();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        controladorMedicions.decodificaMensaxe(topic, message);


        /*String[] subtopics = topic.split("/");
        DispositivoMqtt dispositivoMqtt;

        System.out.println(topic + ": " + message.toString());
        if (subtopics.length == 3) {
            if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && message.toString().equals("online")) {
                online = true;
                System.out.println("O coordinador ZigBee pasa a estar activo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("state") && message.toString().equals("offline")) {
                online = false;
                System.out.println("O coordinador ZigBee pasa a estar inactivo");
            } else if (subtopics[1].equals("bridge") && subtopics[2].equals("config"))
                System.out.println("Configuración do coordinador ZigBee(Topic '" + topic + "'):\n" + message.toString());
            else
                System.err.println("Erro parseando as mensaxes do topic: " + topic);
        } else if (subtopics.length == 2) {
            if (listaDispositivos.containsKey(subtopics[1])) {
                dispositivoMqtt = new DispositivoMqtt(subtopics[1], message, graphiteDao);
            } else {
                dispositivoMqtt = new DispositivoMqtt(subtopics[1], message, graphiteDao);
                //listaDispositivos.put(subtopics[1], dispositivoMqtt);
            }
        }*/
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("Entrega terminada");
    }

}