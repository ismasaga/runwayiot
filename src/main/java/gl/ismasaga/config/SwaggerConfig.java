package gl.ismasaga.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Para abrir swaggerUI atacar /swagger-ui.html
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                /*.securitySchemes(
                    Collections.singletonList(
                        new ApiKey(HttpHeaders.AUTHORIZATION, HttpHeaders.AUTHORIZATION, ApiKeyVehicle.HEADER.getValue())
                    )
                )
                .securityContexts(
                        Collections.singletonList(
                                SecurityContext.builder().securityReferences(
                                        Collections.singletonList(
                                                new SecurityReference(HttpHeaders.AUTHORIZATION, new AuthorizationScope[0])
                                        )
                                ).build()
                        )
                )*/
                .apiInfo(SwaggerConfig.buildApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("gl.ismasaga.runwayIoT"))
                .paths(PathSelectors.any())
                .build();
    }

    private static ApiInfo buildApiInfo() {
        return new ApiInfoBuilder()
                .title("runwayIoT API")
                .description("API REST do servizo runwayIoT que permite interactuar co mesmo")
                .version("0.1.0")
                .contact(
                        new Contact(
                                "Ismael Saborido",
                                "https://gitlab.com/ismasaga",
                                "ismaelsaborido@outlook.es"
                        )
                )
                .build();
    }

}
